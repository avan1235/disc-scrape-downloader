package com.procyk.maciej

import com.procyk.maciej.scrape.extractDownloadData
import com.procyk.maciej.scrape.isHtmlFile
import com.procyk.maciej.util.download
import java.io.File

fun main(args: Array<String>) {
    val workDir = File(".")
    workDir
        .walkTopDown()
        .filter(File::isFile)
        .filter(File::isHtmlFile)
        .map(::extractDownloadData)
        .filterNotNull()
        .forEach { data ->
            println("Downloading folder ${data.folderName}")
            download(data, workDir)
        }
}