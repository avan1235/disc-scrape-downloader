package com.procyk.maciej.scrape

import it.skrape.core.htmlDocument
import it.skrape.selects.Doc
import it.skrape.selects.html5.a
import it.skrape.selects.html5.body
import it.skrape.selects.html5.div
import java.io.File
import java.lang.Exception

data class ScrapedData(val folderName: String, val namesToLinks: List<Pair<String, String>>)

private fun extractHtmlContent(file: File): String {
    val doc = htmlDocument(file) {}
    return doc.text
}

private fun Doc.extractLinks(): Pair<String, List<String>>? {
    var data: Pair<String, List<String>>? = null
    body {
        div {
            withId = "page-container"
            div {
                withClass = "dir-item"
                div {
                    withClass = "data"
                    div {
                        withClass = "name"
                        a {
                            withClass = "pointer"
                            findAll {
                                data = html to eachHref
                            }
                        }
                    }
                }
            }
        }
    }
    return data
}


fun extractDownloadData(file: File): ScrapedData? {

    val dataPair = try {
        val folderName = htmlDocument(file) {}.text.takeWhile { it != ' ' }

        val doc = htmlDocument(file) {}
        Pair(doc.extractLinks(), folderName)
    }
    catch (e: Exception) {
        var i = 0
        val rawHtml = extractHtmlContent(file)
        val folderName = htmlDocument(rawHtml) {}
            .text.dropWhile { it != ' ' }.takeWhile { c -> if (c == '-') { ++i }; i < 2 }
        val doc = htmlDocument(rawHtml) {}
        Pair(doc.extractLinks(), folderName)
    }

    val (data, folderName) = dataPair

    return data?.let { parsed ->
        val names = parsed.first.split("\n")
        val links = parsed.second
        val downloadTargets = names.zip(links)
        ScrapedData(folderName.trim().replace(' ', '_'), downloadTargets)
    }
}

fun File.isHtmlFile(): Boolean = extension == "html" || extension == "htm"