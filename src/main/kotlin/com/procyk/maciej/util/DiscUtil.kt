package com.procyk.maciej.util

import com.procyk.maciej.bash.runCommandToStdAndWait
import com.procyk.maciej.scrape.ScrapedData
import java.io.File
import java.nio.file.Files
import java.nio.file.StandardCopyOption.REPLACE_EXISTING

private fun extractDownloadLink(url: String): String {
    val id = url.split(',')[1].split('-')[1]
    val name = url.split(',')[2].replace('-', '.')

    return "http://stream.freedisc.pl/video/$id/$name"
}

fun generateDownloadCommand(url: String, directory: String = "."): String {
    val movieUrl = extractDownloadLink(url)

    return """aria2c -x4 -d '$directory' --user-agent="Mozilla/5.0 (X11; U; Linux x86_64; pl-PL) AppleWebKit/532.0 (KHTML, like Gecko) Chrome/48.0.2564.116 Safari/549.0" --referer="http://reseton.pl/static/player/v612/jwplayer.flash.swf" -c "$movieUrl""""
}

fun download(scrapedData: ScrapedData, workDir: File) {
    assert(workDir.isDirectory) { "workDir must be a directory" }

    val contentDir = File("${workDir.absolutePath}/${scrapedData.folderName}")
    contentDir.mkdirs()
    scrapedData.namesToLinks.forEach { downloadData ->
        val (name, url) = downloadData
        val currentFiles = contentDir.listFiles()?.map(File::getName)?.toSet()
            ?: throw IllegalStateException("Directory not exists")

        val command = generateDownloadCommand(url, directory = "./${scrapedData.folderName}")
        println("Writing command: $command")

        val scriptFile = File("./download.sh")

        scriptFile.writeText(command)
        "bash ./download.sh".runCommandToStdAndWait(workDir)
        scriptFile.delete()

        val changedFiles = contentDir.listFiles()
        val downloadedFiles = changedFiles?.filter { it.name !in currentFiles }
            ?: throw IllegalStateException("Directory not exists")

        assert(downloadedFiles.size == 1) { "Only single file can be downloaded and appear in folder" }

        val newFile = downloadedFiles[0]
        Files.move(newFile.toPath(), File(newFile.absolutePath.replace(newFile.name, name)).toPath(), REPLACE_EXISTING)
        println("Downloaded ${newFile.absolutePath}")
    }
}