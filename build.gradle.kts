import com.github.jengelman.gradle.plugins.shadow.tasks.ShadowJar

plugins {
    kotlin("jvm") version "1.3.61"
    id("com.github.johnrengelman.shadow") version "5.1.0"
    application
}

group = "com.procyk.maciej"
version = "1.0"

repositories {
    mavenCentral()
    jcenter()
}

dependencies {
    implementation(kotlin("stdlib-jdk8"))
    implementation("it.skrape:skrapeit-core:1.0.0-alpha3")
}

tasks {
    compileKotlin {
        kotlinOptions.jvmTarget = "1.8"
    }
    compileTestKotlin {
        kotlinOptions.jvmTarget = "1.8"
    }
}

tasks.withType<ShadowJar>() {
    manifest {
        attributes["Main-Class"] = "com.procyk.maciej.RunKt"
    }
}

application {
    mainClassName = "com.procyk.maciej.RunKt"
}

